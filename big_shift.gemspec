Gem::Specification.new do |spec|
  spec.name          = 'big_shift'
  spec.version       = '0.0.2'
  spec.authors      << 'Travis Herrick'
  spec.authors      << 'Meagan Cooney'
  spec.email         = ['travish@awesomenesstv.com']
  spec.summary       = 'BigShift API interface'
  spec.description   = '
    Provides an interface for uploading data to BigQuery.
  '.strip

  spec.homepage      = 'https://github.com/awesomenesstv/big_shift'
  spec.license       = 'LGPLv3'
  spec.files         = Dir['lib/**/*.rb', 'license/*']

  spec.extra_rdoc_files = [
    'README.md',
    'license/gplv3.md',
    'license/lgplv3.md',
  ]

  spec.add_dependency 'faraday', '~> 0'
  spec.add_dependency 'reverb',  '~> 0'

  spec.add_development_dependency 'rake_tasks',   '~> 4'
  spec.add_development_dependency 'gems',         '~> 0'
  spec.add_development_dependency 'cane',         '~> 2'
  spec.add_development_dependency 'rspec',        '~> 3'
  spec.add_development_dependency 'vcr',          '~> 2'
  spec.add_development_dependency 'webmock',      '~> 1'
  spec.add_development_dependency 'dotenv',       '~> 1'
  spec.add_development_dependency 'factory_girl', '~> 4'
end
