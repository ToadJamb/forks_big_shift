require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'spec/vcr_cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
  [
    'BIG_SHIFT_GOOGLE_CLIENT_ID',
    'BIG_SHIFT_GOOGLE_CLIENT_SECRET',
    'BIG_SHIFT_REFRESH_TOKEN',
  ].each do |env_var|
    config.filter_sensitive_data("<#{env_var}>") { ENV[env_var] }
  end
end
