require 'spec_helper'

RSpec.describe BigShift::BaseTableCommand do
  subject do
    Class.new(described_class) do
      class << self
        def set_table(table_id)
          self.table_id = table_id
        end

        def base_url_value
          base_url
        end
      end
    end
  end

  let(:project_id)   { 'project-id' }
  let(:dataset_id)   { 'dataset-id' }
  let(:access_token) { 'access-token' }
  let(:table_id)     { 'table-id' }

  before do
    subject.set_table table_id
    allow(BigShift::AccessTokenService)
      .to receive(:retrieve_token)
      .and_return access_token
  end

  describe '.base_url' do
    let(:env) {{
      'BIG_SHIFT_PROJECT_ID' => project_id,
      'BIG_SHIFT_DATASET_ID' => dataset_id,
    }}

    it "uses the base url from #{BigShift::BaseCommand} as a prefix" do
      expect(subject.base_url_value.to_s)
        .to match(%r|googleapis.*/bigquery/v2|)
    end

    it 'includes table information' do
      expect(subject.base_url_value.to_s)
        .to match(%r|/tables/#{table_id}$|)
    end
  end
end
