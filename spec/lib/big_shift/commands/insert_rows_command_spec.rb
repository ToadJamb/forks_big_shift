require 'spec_helper'

RSpec.describe BigShift::InsertRowsCommand do
  it "inherits #{BigShift::BaseTableCommand}" do
    expect(described_class.new).to be_a BigShift::BaseTableCommand
  end

  describe '.body' do
    subject { described_class.body }

    let(:rows) do
      list = []
      5.times do |n|
        list << {:key => "value-#{n}"}
      end
      list
    end

    let(:body_rows) { subject[:rows] }

    before { described_class.instance_variable_set :@rows, rows }

    it 'contains a rows array' do
      expect(subject).to have_key :rows
      expect(body_rows).to be_an Array
      expect(body_rows.count).to be > 0

      body_rows.each_with_index do |body_row_item, i|
        expect(body_row_item).to have_key :json
        expect(body_row_item[:json]).to be_a Hash
        expect(body_row_item[:json]).to eq rows[i]
      end
    end
  end

  describe '.endpoint' do
    subject { described_class.endpoint }
    it 'sets the endpoint correctly' do
      expect(subject).to eq 'insertAll'
    end
  end
end
