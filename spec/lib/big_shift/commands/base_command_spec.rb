require 'spec_helper'

RSpec.describe BigShift::BaseCommand do
  subject do
    Class.new(described_class) do
      class << self
        def connection_value
          connection
        end

        def url_value
          url
        end

        def request_body_value
          request_body
        end

        def header_value
          headers
        end

        private

        def endpoint
          'test-point'
        end

        def body
          {:content => 'body-content'}
        end

        def url_params
          {'foo' => 'bar'}
        end
      end
    end
  end

  let(:access_token) { 'access-token' }

  before do
    allow(BigShift::AccessTokenService)
      .to receive(:retrieve_token)
      .and_return access_token
  end

  describe '.url' do
    let(:project_id) { 'project-id' }
    let(:dataset_id) { 'dataset-id' }

    let(:env) {{
      'BIG_SHIFT_PROJECT_ID' => project_id,
      'BIG_SHIFT_DATASET_ID' => dataset_id,
    }}

    before { stub_const 'ENV', env }

    it 'uses the bigquery api' do
      expect(subject.connection_value.build_url.to_s)
        .to match(%r|googleapis.*/bigquery/v2|)
    end

    it 'includes the project id in the url' do
      expect(subject.connection_value.build_url.to_s)
        .to match(%r|/projects/#{project_id}|)
    end

    it 'includes the dataset id in the url' do
      expect(subject.connection_value.build_url.to_s)
        .to match(%r|/datasets/#{dataset_id}|)
    end

    it 'includes the endpoint' do
      expect(subject.url_value.to_s).to match(%r|/test-point\?|)
    end

    it 'includes the access token as a parameter' do
      expect(subject.url_value.to_s)
        .to match(%r|\?.*key=#{access_token}|)
    end

    it 'includes url parameters' do
      expect(subject.url_value.to_s).to match(%r|\?.*foo=bar|)
    end
  end

  describe '.request_body' do
    it 'includes body content' do
      expect(subject.request_body_value).to eq '{"content":"body-content"}'
    end
  end

  describe '.headers' do
    it 'contains authorization' do
      expect(subject.header_value)
        .to include 'Authorization' => "Bearer #{access_token}"
    end

    it 'contains content type' do
      expect(subject.header_value)
        .to include 'Content-Type' => 'application/json'
    end
  end
end
