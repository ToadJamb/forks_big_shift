require 'spec_helper'

RSpec.describe BigShift::GetAccessTokenCommand do
  let(:env) {{
    'BIG_SHIFT_GOOGLE_CLIENT_ID'     => client_id,
    'BIG_SHIFT_GOOGLE_CLIENT_SECRET' => client_secret,
    'BIG_SHIFT_REFRESH_TOKEN'        => refresh_token,
  }}

  let(:client_id)     { 'the-googs' }
  let(:client_secret) { 'client-secret' }
  let(:refresh_token) { 'refresh-token' }

  before { stub_const 'ENV', env }

  it "inherits #{BigShift::BaseCommand}" do
    expect(described_class.new).to be_a BigShift::BaseCommand
  end

  describe '.params' do
    subject { described_class.params }

    it 'includes the client id' do
      expect(subject).to include :client_id => client_id
    end

    it 'includes the client secret' do
      expect(subject).to include :client_secret => client_secret
    end

    it 'includes the refresh token' do
      expect(subject).to include :refresh_token => refresh_token
    end

    it 'includes the grant type' do
      expect(subject).to include :grant_type => 'refresh_token'
    end
  end

  describe '.headers' do
    subject { described_class.headers }

    it 'sets the content type' do
      expect(subject).to eq 'Content-Type' => 'application/json'
    end
  end

  describe '.endpoint' do
    subject { described_class.endpoint }
    it 'sets the endpoint correctly' do
      expect(subject).to eq 'token'
    end
  end
end
