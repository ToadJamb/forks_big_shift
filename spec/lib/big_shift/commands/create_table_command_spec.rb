require 'spec_helper'

RSpec.describe BigShift::CreateTableCommand do
  it "inherits #{BigShift::BaseCommand}" do
    expect(described_class.new).to be_a BigShift::BaseCommand
  end

  describe '.body' do
    subject { JSON described_class.body.to_json }

    let(:env) {{
      'BIG_SHIFT_PROJECT_ID' => project_id,
      'BIG_SHIFT_DATASET_ID' => dataset_id,
    }}

    let(:project_id) { 'project-id' }
    let(:dataset_id) { 'dataset-id' }
    let(:table_name) { 'tablename' }

    let(:schema) do
      BigShift::Schema.new(table_name)
        .add_field('field1', :string)
        .add_field('field2', :integer)
        .add_field('field3', :boolean)
    end

    before { stub_const 'ENV', env }

    before { described_class.instance_variable_set :@schema, schema }

    it 'contains a table reference hash' do
      expect(subject).to have_key 'tableReference'
    end

    it 'contains a schema hash' do
      expect(subject).to have_key 'schema'
    end

    context 'table reference' do
      let(:table_ref) { subject['tableReference'] }

      it 'includes the project id' do
        expect(table_ref).to include 'projectId' => project_id
      end

      it 'includes the dataset id' do
        expect(table_ref).to include 'datasetId' => dataset_id
      end

      it 'includes the table id' do
        expect(table_ref).to include 'tableId' => table_name
      end
    end

    context 'schema' do
      let(:body_schema) { subject['schema'] }
      let(:schema_fields) { body_schema['fields'] }

      it 'contains a fields array with the specified fields' do
        expect(body_schema).to have_key 'fields'
        expect(schema_fields).to be_an Array
        expect(schema_fields.count).to eq 3
        schema_fields.each do |schema_field|
          expect(schema_field).to be_a Hash
        end

        expect(schema_fields[0]).to eq 'name' => 'field1', 'type' => 'string'
        expect(schema_fields[1]).to eq 'name' => 'field2', 'type' => 'integer'
        expect(schema_fields[2]).to eq 'name' => 'field3', 'type' => 'boolean'
      end
    end
  end

  describe '.endpoint' do
    subject { described_class.endpoint }
    it 'sets the endpoint correctly' do
      expect(subject).to eq 'tables'
    end
  end
end
