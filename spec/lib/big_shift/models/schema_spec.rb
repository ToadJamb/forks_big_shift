require 'spec_helper'

RSpec.describe BigShift::Schema do
  let(:object) do
    described_class.new(table_name)
      .add_field('field1', :string)
      .add_field('field2', :integer)
  end

  let(:table_name) { 'table-name' }

  describe '#fields' do
    subject { object.fields }

    it 'returns the fields' do
      expect(subject).to be_an Array
      expect(subject.count).to eq 2

      subject.each do |field|
        expect(field).to be_a BigShift::TableField
      end
    end
  end

  describe '#to_json' do
    subject { JSON object.to_json }

    context 'fields field' do
      let(:fields_field) { subject['fields'] }

      it 'exists' do
        expect(subject).to have_key 'fields'
      end

      it 'is an array' do
        expect(fields_field).to be_an Array
      end

      it 'contains fields' do
        expect(fields_field.count).to be > 0

        fields_field.each do |field|
          expect(field).to have_key 'name'
          expect(field).to have_key 'type'
        end
      end
    end
  end
end
