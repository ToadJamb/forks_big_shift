require 'spec_helper'

RSpec.describe BigShift::BaseModel do
  subject do
    Class.new(described_class) do
      attr_accessor :foo, :bar

      def as_json(json_state)
        {
          :key => 'value',
        }
      end
    end.new
  end

  describe '#to_json' do
    it 'returns the json version of #as_json' do
      expect(subject.to_json).to eq '{"key":"value"}'
    end
  end
end
