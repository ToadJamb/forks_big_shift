require 'spec_helper'

RSpec.describe BigShift::TableField do
  let(:object) { described_class.new field_name, field_type }

  let(:field_name) { 'field-name' }
  let(:field_type) { :field_type }

  describe '#field_name' do
    subject { object.field_name }
    it 'returns the name of the field' do
      expect(subject).to eq field_name
    end
  end

  describe '#field_type' do
    subject { object.field_type }
    it 'returns the name of the type' do
      expect(subject).to eq field_type
    end
  end

  describe '#to_json' do
    subject { JSON object.to_json }

    context 'name field' do
      it 'exists' do
        expect(subject).to have_key 'name'
      end

      it 'returns the expected value' do
        expect(subject['name']).to eq field_name
      end
    end

    context 'type field' do
      it 'exists' do
        expect(subject).to have_key 'type'
      end

      it 'returns the expected value' do
        expect(subject['type']).to eq field_type.to_s
      end
    end
  end
end
