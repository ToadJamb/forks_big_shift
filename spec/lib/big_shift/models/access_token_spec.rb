require 'spec_helper'

RSpec.describe BigShift::AccessToken do
  let(:hash) {{
    'access_token' => 'access-token',
    'expires_in'   => expires_in,
  }}

  let(:expires_in) { 3600 }

  describe '.new' do
    subject { described_class.new hash }

    shared_examples_for 'sets attribute' do |attr, key, value|
      let(:hash) {{ key => value, }}

      it "uses #{key} to set #{attr} to #{value.inspect}" do
        expect(subject.send(attr)).to eq value
      end
    end

    it_behaves_like 'sets attribute', :access_token, 'access_token', 'token'
    it_behaves_like 'sets attribute', :expires, 'expires_in', 1234
  end

  describe '.expired?' do
    subject { object.expired? }

    let(:object) { described_class.new hash }

    context 'given the expiration time has not passed' do
      it 'returns false' do
        expect(subject).to eq false
      end
    end

    context 'given the expiration time has passed' do
      shared_examples_for 'an expired token' do |ago, expires, value|
        context "given the access token was created #{ago} seconds ago" do
          before { object.instance_variable_set :@created, Time.now - ago }

          context "given the token lives for #{expires} seconds" do
            let(:expires_in) { expires }

            it "returns #{value}" do
              expect(subject).to eq value
            end
          end
        end
      end

      it_behaves_like 'an expired token', 60, 30, true
      it_behaves_like 'an expired token', 10, 30, false

      it_behaves_like 'an expired token', 21, 30, true
      it_behaves_like 'an expired token', 20, 30, true
      it_behaves_like 'an expired token', 19, 30, false

      it_behaves_like 'an expired token', 291, 300, true
      it_behaves_like 'an expired token', 290, 300, true
      it_behaves_like 'an expired token', 289, 300, false
    end
  end

  describe 'factory girl usage' do
    context 'build' do
      subject { build :access_token }

      it 'creates a video object' do
        expect(subject).to be_a BigShift::AccessToken
      end

      shared_examples_for 'sets factory attribute' do |attr, value|
        subject { build :access_token, attr => value }

        it "sets #{attr} to #{value.inspect}" do
          expect(subject.send(attr)).to eq value
        end
      end

      it_behaves_like 'sets factory attribute', :access_token, 'access-token'
      it_behaves_like 'sets factory attribute', :expires, 4231
    end
  end
end
