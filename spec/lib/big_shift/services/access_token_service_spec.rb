require 'spec_helper'

RSpec.describe BigShift::AccessTokenService do
  let(:response) { build :response, :data => access_token }
  let(:access_token) { build :access_token, :access_token => 'token-foo' }

  describe '.retrieve_token' do
    subject { described_class.retrieve_token }

    before { described_class.instance_variable_set :@token, nil }

    context 'given a token does not exist' do
      before { described_class.send :remove_instance_variable, :@token }

      it 'returns a token' do
        expect(BigShift::GetAccessTokenCommand)
          .to receive(:execute)
          .and_return response
        expect(subject).to eq 'token-foo'
      end
    end

    context 'given a token exists' do
      before { described_class.instance_variable_set :@token, access_token }

      context 'given the token is not expired' do
        before { expect(access_token.expired?).to eq false }

        it 'returns the existing token' do
          expect(subject).to eq 'token-foo'
        end
      end

      context 'given the token is expired' do
        let(:expired_token) { build :expired_token }

        before { described_class.instance_variable_set :@token, expired_token }

        before { expect(expired_token.expired?).to eq true }

        it 'returns a token' do
          expect(BigShift::GetAccessTokenCommand)
            .to receive(:execute)
            .and_return response
          expect(subject).to eq 'token-foo'
        end
      end
    end
  end
end
