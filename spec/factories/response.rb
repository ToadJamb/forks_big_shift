FactoryGirl.define do
  factory :response, :class => Reverb::Response do
    status 200
    success true
  end
end
