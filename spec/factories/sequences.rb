FactoryGirl.define do
  sequence(:access_token) { |n| "access-token-#{n}" }
end
