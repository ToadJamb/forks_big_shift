FactoryGirl.define do
  factory :access_token, :class => BigShift::AccessToken do
    access_token { generate :access_token }
    expires 3600

    factory :expired_token do
      expires { -1 }
    end
  end
end
