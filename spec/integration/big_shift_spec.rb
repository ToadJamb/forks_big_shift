require 'spec_helper'

RSpec.describe BigShift, :vcr do
  let(:table_name) { 'insert_table' }

  before { BigShift::AccessTokenService.instance_variable_set :@token, nil }
  before { BigShift::AccessTokenService.remove_instance_variable :@token }

  describe '.create_table' do
    subject { described_class.create_table schema }

    let(:table_name) { 'create_table' }

    let(:schema) do
      BigShift::Schema.new(table_name)
        .add_field('field1', :string)
        .add_field('field2', :integer)
        .add_field('field3', :float)
    end

    it 'creates a table' do
      expect(subject.success?).to eq true
      expect(subject.status).to eq 200
    end
  end

  describe '.insert_rows' do
    context 'given the rows are inserted' do
      subject { described_class.insert_rows table_name, rows }

      let(:rows) {[{
        :field1 => 'item-id-1',
      }, {
        :field1 => 'item-id-2',
      }, {
        :field1 => 'item-id-3',
      }]}

      it 'returns a successful response' do
        expect(subject.success?).to eq true
        expect(subject.status).to eq 200
      end
    end

    # This is necessary because BigQuery returns a 200
    # when there are errors (such as when a field does not exist).
    context 'given the rows are not inserted' do
      subject { described_class.insert_rows table_name, rows }

      let(:rows) {[{
        :foobar => 'item-id-1',
      }]}

      it 'returns an unsuccessful response' do
        expect(subject.status).to eq 200
        expect(subject.success?).to eq false
      end
    end
  end
end
