require 'json'

require 'faraday'
require 'reverb'

module BigShift
end

require_relative 'big_shift/models/base_model'
require_relative 'big_shift/models/access_token'
require_relative 'big_shift/models/table_field'
require_relative 'big_shift/models/schema'

require_relative 'big_shift/responses/get_access_token_response'
require_relative 'big_shift/responses/create_table_response'
require_relative 'big_shift/responses/insert_rows_response'

require_relative 'big_shift/commands/base_command'
require_relative 'big_shift/commands/base_table_command'
require_relative 'big_shift/commands/create_table_command'
require_relative 'big_shift/commands/get_access_token_command'
require_relative 'big_shift/commands/insert_rows_command'

require_relative 'big_shift/services/access_token_service'

require_relative 'big_shift/core'

module BigShift
  extend Core
end
