module BigShift
  class BaseTableCommand < BaseCommand
    class << self
      attr_accessor :table_id

      private

      def base_url
        "#{super}/tables/#{table_id}"
      end
    end
  end
end
