module BigShift
  class BaseCommand
    class << self
      private

      def connection
        Faraday.new(:url => base_url)
      end

      def post
        connection.post url, request_body, headers
      end

      def headers
        {
          'Authorization' => "Bearer #{access_token}",
          'Content-Type' => 'application/json',
        }
      end

      def params
        url_params.merge({
          :key => access_token,
        })
      end

      def request_body
        body.to_json
      end

      def base_url
        'https://www.googleapis.com/bigquery/v2/projects/%s/datasets/%s' % [
          project_id,
          dataset_id,
        ]
      end

      def url
        connection.build_url endpoint, params
      end

      def url_params
        {}
      end

      def body
        {}
      end

      def project_id
        ENV['BIG_SHIFT_PROJECT_ID']
      end

      def dataset_id
        ENV['BIG_SHIFT_DATASET_ID']
      end

      def access_token
        AccessTokenService.retrieve_token
      end
    end
  end
end
