module BigShift
  class GetAccessTokenCommand < BaseCommand
    class << self
      def execute
        GetAccessTokenResponse.new post
      end

      def headers
        {
          'Content-Type' => 'application/json',
        }
      end

      def params
        {
          :client_id     => client_id,
          :client_secret => client_secret,
          :refresh_token => refresh_token,
          :grant_type    => 'refresh_token',
        }
      end

      def endpoint
        'token'
      end

      private

      def client_id
        ENV['BIG_SHIFT_GOOGLE_CLIENT_ID']
      end

      def client_secret
        ENV['BIG_SHIFT_GOOGLE_CLIENT_SECRET']
      end

      def refresh_token
        ENV['BIG_SHIFT_REFRESH_TOKEN']
      end

      def base_url
        'https://www.googleapis.com/oauth2/v3'
      end
    end
  end
end
