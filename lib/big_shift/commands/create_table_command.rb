module BigShift
  class CreateTableCommand < BaseCommand
    class << self
      def execute(schema)
        @schema = schema
        CreateTableResponse.new post
      end

      def endpoint
        'tables'
      end

      def body
        {
          'tableReference' => {
            'projectId' => project_id,
            'datasetId' => dataset_id,
            'tableId'   => @schema.table_name,
          },

          'schema' => @schema,
        }
      end
    end
  end
end
