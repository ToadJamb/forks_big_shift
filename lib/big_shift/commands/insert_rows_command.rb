module BigShift
  class InsertRowsCommand < BaseTableCommand
    class << self
      def execute(table_name, rows)
        @rows = rows
        self.table_id = table_name
        InsertRowsResponse.new post
      end

      def endpoint
        'insertAll'
      end

      def body
        { :rows => build_rows }
      end

      private

      def build_rows
        @rows.map do |row|
          {:json => row}
        end
      end
    end
  end
end
