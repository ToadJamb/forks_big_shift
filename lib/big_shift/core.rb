module BigShift
  module Core
    extend self

    def create_table(table_name)
      CreateTableCommand.execute table_name
    end

    def insert_rows(table_name, rows)
      InsertRowsCommand.execute table_name, rows
    end
  end
end
