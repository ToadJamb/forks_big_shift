module BigShift
  module AccessTokenService
    extend self

    def retrieve_token
      if !defined?(@token) or @token.expired?
        response = GetAccessTokenCommand.execute
        @token = response.data
      end

      @token.access_token
    end
  end
end
