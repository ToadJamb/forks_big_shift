module BigShift
  class BaseModel
    def to_json(json_state = nil)
      as_json(json_state).to_json
    end
  end
end
