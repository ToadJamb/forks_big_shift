module BigShift
  class Schema < BaseModel
    attr_reader :table_name
    attr_reader :fields

    def initialize(table_name)
      @table_name = table_name
      @fields = []
    end

    def add_field(field_name, field_type)
      @fields << TableField.new(field_name, field_type)
      self
    end

    def as_json(json_state = nil)
      {
        'fields' => @fields,
      }
    end
  end
end
