module BigShift
  class TableField < BaseModel
    attr_reader :field_name
    attr_reader :field_type

    def initialize(field_name, field_type)
      @field_name = field_name
      @field_type = field_type
    end

    def as_json(json_state = nil)
      {
        :name => @field_name,
        :type => @field_type,
      }
    end
  end
end
