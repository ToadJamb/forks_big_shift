module BigShift
  class AccessToken
    attr_accessor :access_token, :expires

    def initialize(hash = {})
      @access_token = hash['access_token']
      @expires      = hash['expires_in']
      @created      = Time.now
    end

    def expired?
      Time.now - @created > (expires - 10)
    end
  end
end
