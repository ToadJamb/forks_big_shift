module BigShift
  class GetAccessTokenResponse < Reverb::Response
    def on_success
      self.data = AccessToken.new(body)
    end
  end
end
