module BigShift
  class InsertRowsResponse < Reverb::Response
    def on_success
      if body['insertErrors']
        @success = false
      end
    end
  end
end
